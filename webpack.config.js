const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'jobtech-taxonomy-autocomplete-occupation-name.js',
    globalObject: 'this',
    library: {
      name: 'jobtechTaxonomyAutocompleteOccupationName',
      type: 'umd',
    },
  },
};
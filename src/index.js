import occupationNameIndex from './occupationNameIndex.json';

export function autocompleteOccupationName (s){

    if(s){

    return occupationNameIndex.filter(indexRow =>
         {
             return (-1 < indexRow[1].search(s)) || (-1 < indexRow[2].search(s));
         
         }
     ).map(indexRow => {
         return { id: indexRow[0], label: indexRow[1]};
     });
    } else {
        return [];

    } 
 }

 console.log();